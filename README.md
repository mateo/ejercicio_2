Ejercicio 1 de la practica 2

Primer commit en la rama main:
   Agregar un archivo llamado estudiante.txt.
   El archivo debe contener una sola línea con tu nombre de usuario de dominio único URJC (sin el @alumnos.urjc.es).

Segundo commit en la rama main:
   Renombrar/mover el archivo estudiante.txt a username.txt.
   Añadir un nuevo archivo llamado README.md.
   El archivo README.md debe contener una pequeña descripción del objetivo del repositorio.

Tercer commit en la rama main:
   Añadir un nuevo archivo llamado nachos.recipe.
   El archivo nachos.recipe debe contener una sola línea con la URL de una página de Internet que explique una receta para hacer nachos con guacamole.
   Añadir un archivo de imagen que represente el plato preparado.
   La ruta al archivo de imagen debe ser img/nachos.jpg (o la extensión que prefieras).
   La imagen debe almacenarse en un subdirectorio img dentro del directorio raíz del repositorio.
